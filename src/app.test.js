const { loadApp } = require('./app');

describe('loadApp', () => {
  it('returns instance of express app', () => {
    // arrange
    const app = loadApp();

    // assert
    expect(app.use).toBeInstanceOf(Function);
    expect(app.get).toBeInstanceOf(Function);
    expect(app.post).toBeInstanceOf(Function);
    expect(app.patch).toBeInstanceOf(Function);
    expect(app.put).toBeInstanceOf(Function);
    expect(app.delete).toBeInstanceOf(Function);
    expect(app.options).toBeInstanceOf(Function);
    expect(app.disable).toBeInstanceOf(Function);
    expect(app.enable).toBeInstanceOf(Function);
  });
});
