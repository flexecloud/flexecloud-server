const express = require('express');
const supertest = require('supertest');
const bodyParser = require('body-parser');
const trim = require('./trim');

const echoEndpoint = (req, res) => res.status(200).json(req.body);

describe('trimStrings middleware', () => {
  it('removes trailing whitespaces and new lines', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(trim)
      .post('/', echoEndpoint);
    const body = { test: 'Trim this! \n' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.body).toEqual({ test: 'Trim this!' });
        expect(res.status).toBe(status);

        done(err);
      });
  });

  it('removes leading whitespaces and new lines', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(trim)
      .post('/', echoEndpoint);
    const body = { test: '\n Trim this!' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.body).toEqual({ test: 'Trim this!' });
        expect(res.status).toBe(status);

        done(err);
      });
  });

  it('trims recursively', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(trim)
      .post('/', echoEndpoint);
    const body = {
      test: '\n Trim this!',
      notThis: {
        test: '\n Please trim me! \n'
      }
    };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.body).toEqual({
          test: 'Trim this!',
          notThis: {
            test: 'Please trim me!'
          }
        });
        expect(res.status).toBe(status);

        done(err);
      });
  });
});
