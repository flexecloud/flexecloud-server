module.exports = (req, res) =>
  res.status(404).json({
    error: res.t('endpoint_not_supported')
  });
