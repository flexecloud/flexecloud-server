const passport = require('../passport');
const { AZURE_AD_DOMAIN } = require('../../common/env');

const options = {
  session: false,
  domain_hint: AZURE_AD_DOMAIN
};

module.exports = passport.authenticate('azureAd', options);
