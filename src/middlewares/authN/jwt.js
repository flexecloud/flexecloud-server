const passport = require('../passport');

module.exports = (req, res, next) => {
  const options = { session: false };
  passport.authenticate('jwt', options, (err, user, info) => {
    if (err) {
      return next(err);
    }
    if (info) {
      if (info.message === 'No auth token') {
        return res.status(401).json({
          error: res.t('auth_error_token_required')
        });
      }
      if (info.message === 'jwt expired') {
        return res.status(403).json({
          error: res.t('auth_error_token_expired')
        });
      }
    }
    req.user = user;
    return next();
  })(req, res, next);
};
