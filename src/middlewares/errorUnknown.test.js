const express = require('express');
const supertest = require('supertest');
const errorUnknown = require('./errorUnknown');

const i18nMiddleware = (req, res, next) => {
  res.t = string => string;
  return next();
};

const faultyController = (req, res, next) => next(new Error('This is a test'));

describe('errorUnknown middleware', () => {
  it('returns status code 500 and an error message', done => {
    // arrange
    const app = express()
      .use(i18nMiddleware)
      .get('/error', faultyController)
      .use(errorUnknown);
    const body = { error: 'error_unknown' };
    const status = 500;

    // act
    supertest(app)
      .get('/error')
      .end((err, res) => {
        // assert
        expect(res.body).toEqual(body);
        expect(res.status).toBe(status);

        done(err);
      });
  });
});
