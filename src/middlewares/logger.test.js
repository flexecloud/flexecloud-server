const { EventEmitter } = require('events');
const logger = require('./logger');

describe('logger middleware', () => {
  it('calls next spy', () => {
    // arrange
    const nextSpy = jest.fn();
    const reqMock = {
      baseUrl: '/api',
      url: '/v1',
      method: 'GET'
    };
    const resMock = {
      statusCode: 200,
      on: jest.fn()
    };
    const loggerMock = { info: jest.fn() };
    logger.__Rewire__('logger', loggerMock);

    // act
    logger(reqMock, resMock, nextSpy);

    // assert
    expect(nextSpy).toHaveBeenCalled();

    // teardown
    logger.__ResetDependency__('logger');
  });

  it('calls Date.now only once without finish event', () => {
    // arrange
    const _Date = global.Date;
    global.Date.now = jest.fn();
    const reqMock = {
      baseUrl: '/api',
      url: '/v1',
      method: 'GET'
    };
    const resMock = {
      statusCode: 200,
      on: jest.fn()
    };
    const nextSpy = jest.fn();
    const loggerMock = { info: jest.fn() };
    logger.__Rewire__('logger', loggerMock);

    // act
    logger(reqMock, resMock, nextSpy);

    // assert
    expect(global.Date.now).toHaveBeenCalledTimes(1);

    // teardown
    global.Date = _Date;
    logger.__ResetDependency__('logger');
  });

  it('calls Date.now twice when finish event is emitted', () => {
    // arrange
    const _Date = global.Date;
    global.Date.now = jest.fn();
    const nextSpy = jest.fn();
    const reqMock = {
      baseUrl: '/api',
      url: '/v1/health',
      method: 'GET'
    };
    const resMock = new EventEmitter();
    resMock.statusCode = 200;
    const loggerMock = { info: jest.fn() };
    logger.__Rewire__('logger', loggerMock);

    // act
    logger(reqMock, resMock, nextSpy);
    resMock.emit('finish');

    // assert
    expect(global.Date.now).toHaveBeenCalledTimes(2);

    // teardown
    global.Date = _Date;
    logger.__ResetDependency__('logger');
  });

  it('logs proper endpoint without baseUrl', () => {
    // arrange
    const _Date = global.Date;
    global.Date.now = jest
      .fn()
      .mockReturnValueOnce(20)
      .mockReturnValueOnce(220);
    const nextSpy = jest.fn();
    const reqMock = {
      url: '/api/v1',
      method: 'GET'
    };
    const resMock = new EventEmitter();
    resMock.statusCode = 200;
    const loggerMock = { info: jest.fn() };
    logger.__Rewire__('logger', loggerMock);

    // act
    logger(reqMock, resMock, nextSpy);
    resMock.emit('finish');

    // assert
    expect(loggerMock.info).toHaveBeenCalledWith('[API] 200 GET /api/v1 200ms');

    // teardown
    global.Date = _Date;
    logger.__ResetDependency__('logger');
  });

  it('logs proper endpoint without url', () => {
    // arrange
    const _Date = global.Date;
    global.Date.now = jest
      .fn()
      .mockReturnValueOnce(20)
      .mockReturnValueOnce(220);
    const nextSpy = jest.fn();
    const reqMock = {
      baseUrl: '/api/v1',
      method: 'GET'
    };
    const resMock = new EventEmitter();
    resMock.statusCode = 200;
    const loggerMock = { info: jest.fn() };
    logger.__Rewire__('logger', loggerMock);

    // act
    logger(reqMock, resMock, nextSpy);
    resMock.emit('finish');

    // assert
    expect(loggerMock.info).toHaveBeenCalledWith('[API] 200 GET /api/v1 200ms');

    // teardown
    global.Date = _Date;
    logger.__ResetDependency__('logger');
  });
});
