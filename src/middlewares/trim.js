const { trim, isObjectLike } = require('lodash');

const walkValues = func => obj => {
  if (isObjectLike(obj)) {
    if (Array.isArray(obj)) {
      return obj.map(walkValues(func));
    }
    return Object.keys(obj).reduce((prev, key) => {
      const next = prev;
      const value = obj[key];
      if (isObjectLike(value)) {
        next[key] = walkValues(func)(value);
      } else {
        next[key] = func(value);
      }
      return next;
    }, {});
  }
  return obj;
};

module.exports = (req, res, next) => {
  req.body = walkValues(trim)(req.body);
  return next();
};
