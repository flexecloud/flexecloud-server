const express = require('express');
const supertest = require('supertest');
const bodyParser = require('body-parser');
const defined = require('./defined');

const echoController = (req, res) => res.status(200).json(req.body);
const i18nMiddleware = (req, res, next) => {
  res.tmf = jest.fn(string => string);
  res.t = jest.fn(string => string);
  return next();
};

describe('defined middleware', () => {
  it('returns status code 400 and error message if body field is undefined', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = {};
    const status = 400;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual({
          error: 'validation_field_required'
        });

        done(err);
      });
  });

  it('calls next controller if body field is string', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = { test: 'a' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        done(err);
      });
  });

  it('calls next controller if body field is number', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = { test: 12 };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        done(err);
      });
  });

  it('calls next controller if body field is null', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = { test: 12 };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        done(err);
      });
  });

  it('calls next controller if body field is object', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = { test: { some: 'data' } };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        done(err);
      });
  });

  it('calls next controller if body field is boolean', done => {
    // arrange
    const app = express()
      .use(bodyParser.json())
      .use(i18nMiddleware)
      .use(defined(['test']))
      .post('/', echoController);
    const body = { test: true };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        done(err);
      });
  });
});
