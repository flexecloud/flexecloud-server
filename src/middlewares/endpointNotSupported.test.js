const express = require('express');
const supertest = require('supertest');
const endpointNotSupported = require('./endpointNotSupported');

const i18nMiddleware = (req, res, next) => {
  res.t = string => string;
  return next();
};

describe('endpointNotSupported middleware', () => {
  it('returns status code 404 and an error message', done => {
    // arrange
    const app = express()
      .use(i18nMiddleware)
      .use(endpointNotSupported);
    const body = { error: 'endpoint_not_supported' };
    const status = 404;

    // act
    supertest(app)
      .get('/')
      .end((err, res) => {
        // assert
        expect(res.body).toEqual(body);
        expect(res.status).toBe(status);

        done(err);
      });
  });
});
