const express = require('express');
const supertest = require('supertest');
const middlewares = require('./index');

const loggerMiddleware = (req, res, next) => next();
const echoEndpoint = (req, res) => res.status(200).json(req.body);
const i18nEndpoint = (req, res) =>
  res.status(200).json({ message: res.t('error_unknown') });

describe('loadSetupMiddlewares', () => {
  it('loads the cors middlware', done => {
    // arrange
    middlewares.__Rewire__('logger', loggerMiddleware);
    const app = express();
    middlewares.loadSetupMiddlewares(app);
    app.post('/', echoEndpoint);
    const body = { test: 'This is a test!' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.headers['access-control-allow-origin']).toBe('*');
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        // teardown
        middlewares.__ResetDependency__('logger');

        done(err);
      });
  });

  it('loads the helmet middlware', done => {
    // arrange
    middlewares.__Rewire__('logger', loggerMiddleware);
    const app = express();
    middlewares.loadSetupMiddlewares(app);
    app.post('/', echoEndpoint);
    const body = { test: 'This is a test!' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.headers['x-powered-by']).toBeUndefined();
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        // teardown
        middlewares.__ResetDependency__('logger');

        done(err);
      });
  });

  it('loads the i18n middlware', done => {
    // arrange
    middlewares.__Rewire__('logger', loggerMiddleware);
    const app = express();
    middlewares.loadSetupMiddlewares(app);
    app.post('/', echoEndpoint);
    const body = { test: 'This is a test!' };
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(res.status).toBe(status);
        expect(res.body).toEqual(body);

        // teardown
        middlewares.__ResetDependency__('logger');

        done(err);
      });
  });

  it('loads the logger middleware', done => {
    // arrange
    const spy = jest.fn();
    const loggerMock = (req, res, next) => {
      res.on('finish', () => spy());
      return next();
    };
    middlewares.__Rewire__('logger', loggerMock);
    const app = express();
    middlewares.loadSetupMiddlewares(app);
    app.post('/', i18nEndpoint);
    const body = {};
    const status = 200;

    // act
    supertest(app)
      .post('/')
      .send(body)
      .end((err, res) => {
        // assert
        expect(spy).toHaveBeenCalled();
        expect(res.status).toBe(status);
        expect(res.body).toEqual({ message: 'An unknown error occurred.' });

        // teardown
        middlewares.__ResetDependency__('logger');

        done(err);
      });
  });
});

describe('loadErrorMiddlewares', () => {});
