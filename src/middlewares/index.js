const cors = require('cors');
const i18n = require('i18n');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');
const logger = require('./logger');
const errorUnknown = require('./errorUnknown');
const endpointNotSupported = require('./endpointNotSupported');
const logError = require('./logError');
const passport = require('./passport');

i18n.configure({
  locales: ['en'],
  directory: './src/locales',
  api: {
    __: 't',
    __n: 'tn',
    __mf: 'tmf',
    __l: 'tl',
    __h: 'th'
  }
});

exports.loadSetupMiddlewares = app => {
  app.use(cors());
  app.use(helmet());
  app.use(i18n.init);
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());
  app.use(passport.initialize());
  app.use(logger);
};

exports.loadErrorMiddlewares = app => {
  app.use(logError);
  app.use(errorUnknown);
  app.use(endpointNotSupported);
};
