const express = require('express');
const supertest = require('supertest');
const logError = require('./logError');

const faultyController = (req, res, next) => next(new Error('This is a test!'));
const errorController = (err, req, res, next) => {
  res.status(500).json(null);
  return next();
};

describe('logError middleware', () => {
  it('logs the error and replaces tabs', done => {
    // arrange
    const loggerMock = { error: jest.fn() };
    logError.__Rewire__('logger', loggerMock);
    const app = express()
      .get('/error', faultyController)
      .use(logError)
      .use(errorController);
    const tabRegex = /\t/g;
    const errorMessage = 'Error: This is a test!';

    // act
    supertest(app)
      .get('/error')
      .end(err => {
        // assert
        const loggedMessage = loggerMock.error.mock.calls[0][0];
        expect(loggedMessage.substring(0, 22)).toBe(errorMessage);
        expect(tabRegex.test(loggedMessage)).toBe(false);

        // teardown
        logError.__ResetDependency__('logger');

        done(err);
      });
  });
});
