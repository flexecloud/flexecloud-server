const asyncMiddleware = require('express-async-handler');
const User = require('../../services/user/model');

module.exports = roles =>
  asyncMiddleware(async (req, res, next) => {
    if (!roles) {
      const errorMessage = 'The roles array must not be undefined or empty.';
      return next(new Error(errorMessage));
    }

    if (!(req.user && req.user.user && req.user.user.id)) {
      return res.status(403).json({
        error: res.tmf('auth_error_identity_model_required', {
          model: res.t('user')
        })
      });
    }

    const user = await User.findById(req.user.user.id);

    if (!roles.includes(user.role)) {
      return res.status(403).json({
        error: res.tmf('auth_error_role_required', {
          items: roles.join(', ')
        })
      });
    }

    return next();
  });
