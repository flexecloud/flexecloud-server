module.exports = identityType => (req, res, next) => {
  if (!(req.user && req.user[identityType] && req.user[identityType].id)) {
    return res.status(403).json({
      error: res.tmf('auth_error_identity_model_required', {
        model: res.t(identityType)
      })
    });
  }
  return next();
};
