// four parameters are required to be recognized as error handler
// eslint-disable-next-line no-unused-vars
module.exports = (error, req, res, next) =>
  res.status(500).json({
    error: res.t('error_unknown')
  });
