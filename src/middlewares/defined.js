const { isUndefined } = require('lodash');

module.exports = fields => (req, res, next) => {
  const invalidField = fields.find(field => isUndefined(req.body[field]));
  if (invalidField) {
    return res.status(400).json({
      error: res.tmf('validation_field_required', {
        field: res.t(invalidField)
      })
    });
  }
  return next();
};
