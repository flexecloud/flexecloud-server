const { config } = require('dotenv');
const logger = require('./logger');

config();

const defaultEnv = {
  MONGODB_URL: 'mongodb://localhost/test',
  PORT: '9000',
  TAG: 'dev',
  APP_URL: 'http://localhost:9000',
  WEBAPP_URL: 'http://localhost:8080',
  JWT_KEY: 'test',
  AZURE_AD_CLIENT_ID: 'clientId',
  AZURE_AD_CLIENT_SECRET: 'clientSecret',
  AZURE_AD_TENANT: 'tenant',
  AZURE_AD_COOKIE_KEY_32: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
  AZURE_AD_COOKIE_KEY_12: 'xxxxxxxxxxxx',
  AZURE_AD_DOMAIN: 'example.com',
  AZURE_AD_ATTRIBUTE_CARD_NUMBER: 'cardNumber',
  AZURE_AD_ATTRIBUTE_CARD_ID: 'cardId',
  GITLAB_API_URL: 'https://gitlab.com',
  GITLAB_API_TOKEN: 'gitlabApiToken',
  GITLAB_PROJECT: 'test/test-project'
};

const envVar = Object.keys(defaultEnv).find(variable => !process.env[variable]);
if (envVar && process.env.NODE_ENV !== 'test') {
  logger.error(`[API] Environment variable ${envVar} must not be undefined.`);
  logger.error('[API] Stopping server ...');
  process.exit(1);
}

const env = Object.keys(defaultEnv).reduce((total, variable) => {
  const obj = total;
  obj[variable] = process.env[variable] || defaultEnv[variable];
  return obj;
}, {});

module.exports = env;
