const mongoose = require('mongoose');
const sanitizePlugin = require('mongoose-sanitize-json');
const mongoosePaginate = require('mongoose-paginate-v2');

mongoose.plugin(sanitizePlugin);
mongoose.plugin(schema => {
  schema.set('timestamps', {
    createdAt: 'created',
    updatedAt: 'updated'
  });
});

mongoose.plugin(mongoosePaginate);

module.exports = mongoose;
