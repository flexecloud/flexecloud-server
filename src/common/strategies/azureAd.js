const { get } = require('axios');
const { OIDCStrategy } = require('passport-azure-ad');
const {
  APP_URL,
  AZURE_AD_TENANT,
  AZURE_AD_CLIENT_ID,
  AZURE_AD_CLIENT_SECRET,
  AZURE_AD_COOKIE_KEY_32,
  AZURE_AD_COOKIE_KEY_12,
  AZURE_AD_ATTRIBUTE_CARD_NUMBER,
  AZURE_AD_ATTRIBUTE_CARD_ID
} = require('../env');

const fields = [
  AZURE_AD_ATTRIBUTE_CARD_NUMBER,
  AZURE_AD_ATTRIBUTE_CARD_ID,
  'id',
  'mail',
  'givenName',
  'surname',
  'displayName'
].join(',');
const profileEndpoint = `https://graph.microsoft.com/v1.0/me?$select=${fields}`;
const options = {
  identityMetadata: `https://login.microsoftonline.com/${AZURE_AD_TENANT}/v2.0/.well-known/openid-configuration`,
  clientID: AZURE_AD_CLIENT_ID,
  clientSecret: AZURE_AD_CLIENT_SECRET,
  responseType: 'id_token code',
  responseMode: 'form_post',
  redirectUrl: `${APP_URL}/v1/auth/azure_ad/hook`,
  passReqToCallback: false,
  allowHttpForRedirectUrl: true,
  useCookieInsteadOfSession: true,
  nonceLifetime: 600,
  nonceMaxAmount: 5,
  loggingLevel: 'error',
  scope: ['openid'],
  cookieEncryptionKeys: [
    {
      key: AZURE_AD_COOKIE_KEY_32,
      iv: AZURE_AD_COOKIE_KEY_12
    }
  ]
};
const getUser = async (issuer, sub, profile, aToken, rToken, done) => {
  try {
    const res = await get(profileEndpoint, {
      headers: {
        Authorization: `Bearer ${aToken}`
      }
    });
    done(null, { user: res.data });
  } catch (err) {
    done(new Error('Communication to identity provider failed.'), null);
  }
};

module.exports = new OIDCStrategy(options, getUser);
