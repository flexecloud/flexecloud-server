const { Strategy, ExtractJwt } = require('passport-jwt');
const { JWT_KEY } = require('../env');

const BearerExtractor = ExtractJwt.fromAuthHeaderAsBearerToken();
const JwtExtractor = ExtractJwt.fromAuthHeaderWithScheme('JWT');
const options = {
  secretOrKey: JWT_KEY,
  jwtFromRequest: ExtractJwt.fromExtractors([BearerExtractor, JwtExtractor])
};
const getUser = (payload, done) => done(null, payload);

module.exports = new Strategy(options, getUser);
