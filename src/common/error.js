const logger = require('./logger');

exports.formatError = err => `${err.message}\n${err.stack.replace(/\t/, ' ')}`;
exports.exitIfError = (err, scope, exitCode = 1) => {
  if (err) {
    logger.error(`[${scope}] Error: ${exports.formatError(err)}`);
    logger.error('[API] Stopping server ...');
    process.exit(exitCode);
  }
};
