const service = require('express').Router();
const { upsertFromAzureAd } = require('./controllers');
const azureAdAuthN = require('../../middlewares/authN/azureAd');
const identityType = require('../../middlewares/authZ/identityType');

const isUser = identityType('user');

module.exports = () => {
  service.get('/auth/azure_ad', azureAdAuthN);
  service.post('/auth/azure_ad', azureAdAuthN, isUser, upsertFromAzureAd);
  service.post('/auth/azure_ad/hook', azureAdAuthN, isUser, upsertFromAzureAd);

  return service;
};
