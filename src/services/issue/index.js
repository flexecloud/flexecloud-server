const service = require('express').Router();
const { createOne } = require('./controllers');
const trim = require('../../middlewares/trim');
const defined = require('../../middlewares/defined');
const jwtAuthN = require('../../middlewares/authN/jwt');
const identityType = require('../../middlewares/authZ/identityType');

const isUser = identityType('user');
const createOneSchema = defined(['title', 'type', 'description']);

module.exports = () => {
  service.post('/issues', jwtAuthN, isUser, createOneSchema, trim, createOne);

  return service;
};
