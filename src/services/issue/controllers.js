const asyncMiddleware = require('express-async-handler');
const { Issues } = require('gitlab');
const {
  GITLAB_API_URL,
  GITLAB_API_TOKEN,
  GITLAB_PROJECT
} = require('../../common/env');

const minimalLength = {
  title: 10,
  description: 20
};
const types = ['kind/feature', 'kind/bug'];
const issues = new Issues({
  url: GITLAB_API_URL,
  token: GITLAB_API_TOKEN
});

exports.createOne = asyncMiddleware(async (req, res) => {
  if (req.body.title.length < minimalLength.title) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount_min', {
        field: 'title',
        items: res.tn('%d character', minimalLength.title)
      })
    });
  }

  if (req.body.description.length < minimalLength.description) {
    return res.status(400).json({
      error: res.tmf('validation_field_amount_min', {
        field: 'description',
        items: res.tn('%d character', minimalLength.description)
      })
    });
  }

  if (!types.includes(req.body.type)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'type',
        items: types.join(', ')
      })
    });
  }

  let description = '';
  description += `##### User information\n`;
  description += `\n`;
  description += `**User:** ${req.user.user.display_name}  \n`;
  description += `**Email:** ${req.user.user.email}  \n`;
  description += `\n`;
  description += `##### Description\n`;
  description += `\n`;
  description += req.body.description;

  const issueInfo = {
    title: req.body.title,
    labels: ['area/development', req.body.type, 'status/new'].join(','),
    description
  };

  const issue = await issues.create(GITLAB_PROJECT, issueInfo);

  return res.status(201).json({
    data: {
      title: issue.title,
      labels: issue.labels,
      description: issue.description,
      link: issue.web_url
    }
  });
});
