const asyncMiddleware = require('express-async-handler');
const { isMongoId, isEmail } = require('validator');
const User = require('./model');

exports.getOwn = asyncMiddleware(async (req, res) => {
  const user = await User.findById(req.user.user.id);
  return res.status(200).json({ data: user });
});

exports.getOne = asyncMiddleware(async (req, res) => {
  const { userId } = req.params;

  if (isMongoId(userId)) {
    const user = await User.findById(userId);
    if (user) {
      return res.status(200).json({
        data: user
      });
    }
  }

  if (isEmail(userId)) {
    const user = await User.findOne({ email: userId });
    if (user) {
      return res.status(200).json({
        data: user
      });
    }
  }

  const user = await User.findOne({ card_id: userId });
  if (!user) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'user'
      })
    });
  }

  return res.status(200).json({
    data: user
  });
});

exports.getMany = asyncMiddleware(async (req, res) => {
  const limit = parseInt(req.query.limit, 10) || 100;
  const offset = parseInt(req.query.offset, 10) || 0;
  const sortOrder = req.query.sort_order || 'asc';
  const sortBy = req.query.sort_by || 'created';
  const allowedSortOrder = ['asc', 'desc'];
  const allowedSortBy = [
    'created',
    'first_name',
    'display_name',
    'last_name',
    'role',
    'updated'
  ];

  if (sortBy && Array.isArray(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_by'
      })
    });
  }

  if (sortOrder && Array.isArray(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_field_not_unique', {
        field: 'sort_order'
      })
    });
  }

  if (sortOrder && !allowedSortOrder.includes(sortOrder)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_order',
        items: allowedSortOrder.join(', ')
      })
    });
  }

  if (sortBy && !allowedSortBy.includes(sortBy)) {
    return res.status(400).json({
      error: res.tmf('validation_query_value_out_of_range', {
        field: 'sort_by',
        items: allowedSortBy.join(', ')
      })
    });
  }

  const options = { sort: { [sortBy]: sortOrder }, offset, limit };
  const { docs, totalDocs, ...metadata } = await User.paginate({}, options);
  return res.status(200).json({
    data: docs,
    count: totalDocs,
    ...metadata
  });
});

exports.updateOne = asyncMiddleware(async (req, res) => {
  const { userId } = req.params;
  const { permissions, role } = req.body;
  const update = req.body;
  const allowedFields = ['permissions', 'role'];
  const allowedRoles = User.roles;
  const allowedPermissions = User.permissions;
  const fields = Object.keys(update);

  const invalidField = fields.find(field => !allowedFields.includes(field));
  if (fields.length && invalidField) {
    return res.status(400).json({
      error: res.tmf('validation_field_not_editable', {
        field: invalidField
      })
    });
  }

  if (role && !allowedRoles.includes(req.body.role)) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'role',
        items: allowedRoles.join(', ')
      })
    });
  }

  const invalidPermission = permissions.find(
    perm => !allowedPermissions.includes(perm)
  );
  if (permissions && invalidPermission) {
    return res.status(400).json({
      error: res.tmf('validation_field_value_out_of_range', {
        field: 'permissions',
        items: allowedPermissions.join(', ')
      })
    });
  }

  const options = { new: true };
  const user = await User.findByIdAndUpdate(userId, update, options);

  return res.status(200).json({
    data: user
  });
});

exports.deleteOne = asyncMiddleware(async (req, res) => {
  const { userId } = req.params;

  if (req.user.user.id !== userId) {
    const user = await User.findById(req.user.id);
    if (!user) {
      return res.status(401).status({
        error: res.tmf('auth_error_identity_model_required', {
          identity: 'user'
        })
      });
    }
    if (!user.role === 'admin') {
      return res.status(403).status({
        error: res.tmf('auth_error_role_required', {
          items: 'admin'
        })
      });
    }
  }

  const user = await User.findByIdAndRemove(userId);
  if (!user) {
    return res.status(404).json({
      error: res.tmf('error_entity_not_found', {
        entity: 'user'
      })
    });
  }

  return res.status(204).json(null);
});
