const mongoose = require('../../common/mongoose');

const { Schema } = mongoose;
const roles = ['user', 'staff', 'supervisor', 'admin'];
const [defaultRole] = roles;
const permissions = [
  'fdm_3d_printer',
  'sla_3d_printer',
  'dlp_3d_printer',
  'laser_cutter',
  'cnc_milling_machine',
  'cnc_router',
  'cnc_lathe'
];
const defaultPermissions = [];

const userSchema = new Schema({
  azure_id: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  card_id: {
    type: String,
    unique: true,
    required: true
  },
  card_number: {
    type: String,
    required: true
  },
  first_name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  display_name: {
    type: String,
    required: true
  },
  role: {
    type: String,
    enum: roles,
    default: defaultRole,
    required: true
  },
  permissions: [
    {
      type: String,
      enum: permissions,
      default: defaultPermissions
    }
  ]
});

userSchema.statics.roles = roles;
userSchema.statics.defaultRole = defaultRole;
userSchema.statics.permissions = permissions;
userSchema.statics.defaultPermissions = defaultPermissions;

module.exports = mongoose.model('user', userSchema);
