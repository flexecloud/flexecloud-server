const service = require('express').Router();
const {
  getOwn,
  getMany,
  updateOne,
  getOne,
  deleteOne
} = require('./controllers');
const trim = require('../../middlewares/trim');
const jwtAuthN = require('../../middlewares/authN/jwt');
const identityType = require('../../middlewares/authZ/identityType');
const userRole = require('../../middlewares/authZ/userRole');

const isUser = identityType('user');
const isStaff = userRole(['staff', 'supervisor', 'admin']);
const isAdmin = userRole(['admin']);

module.exports = () => {
  service.get('/me', jwtAuthN, isUser, getOwn);
  service.get('/users', jwtAuthN, isStaff, getMany);
  service.get('/users/:userId', jwtAuthN, isStaff, getOne);
  service.patch('/users/:userId', jwtAuthN, isAdmin, trim, updateOne);
  service.delete('/users/:userId', jwtAuthN, isUser, deleteOne);

  return service;
};
