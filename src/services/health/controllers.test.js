const express = require('express');
const supertest = require('supertest');
const controllers = require('./controllers');

describe('health controllers', () => {
  describe('health', () => {
    it('returns status code 200 and information about the server', done => {
      // arrange
      const tag = 'test';
      const statusCode = 200;
      const iso8601Regex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
      controllers.__Rewire__('TAG', tag);
      const app = express().use('/', controllers.health);

      // act
      supertest(app)
        .get('/')
        .end((err, res) => {
          // assert
          expect(res.body.tag).toEqual(tag);
          expect(res.body.started).toMatch(iso8601Regex);
          expect(res.status).toBe(statusCode);

          // teardown
          controllers.__ResetDependency__('TAG');

          done(err);
        });
    });
  });
});
