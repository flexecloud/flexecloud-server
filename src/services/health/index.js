const service = require('express').Router();
const controllers = require('./controllers');

module.exports = () => {
  service.get('/health', controllers.health);

  return service;
};
