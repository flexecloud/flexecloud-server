const express = require('express');
const supertest = require('supertest');
const service = require('./index');

describe('health service', () => {
  it('exposes GET /health', done => {
    // arrange
    const statusCode = 200;
    const tag = 'dev';
    const iso8601Regex = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
    const app = express().use(service());

    // act
    supertest(app)
      .get('/health')
      .end((err, res) => {
        // assert
        expect(res.body.tag).toEqual(tag);
        expect(res.body.started).toMatch(iso8601Regex);
        expect(res.status).toBe(statusCode);

        done(err);
      });
  });
});
