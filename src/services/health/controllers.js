const { TAG } = require('../../common/env');

const startTimestamp = new Date().toISOString();

exports.health = (req, res) =>
  res.status(200).json({
    tag: TAG,
    started: startTimestamp
  });
