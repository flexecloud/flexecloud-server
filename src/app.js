const express = require('express');
const { loadSetupMiddlewares, loadErrorMiddlewares } = require('./middlewares');
const healthService = require('./services/health');
const authService = require('./services/auth');
const userService = require('./services/user');
const issueService = require('./services/issue');

const app = express();

exports.loadApp = () => {
  // load middlewares
  loadSetupMiddlewares(app);

  // load controllers
  app.use('/v1', healthService());
  app.use('/v1', authService());
  app.use('/v1', userService());
  app.use('/v1', issueService());

  // load special middleware
  loadErrorMiddlewares(app);

  return app;
};
