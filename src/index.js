const { loadApp } = require('./app');
const mongoose = require('./common/mongoose');
const logger = require('./common/logger');
const { exitIfError } = require('./common/error');
const { PORT, MONGODB_URL, TAG } = require('./common/env');

const port = parseInt(PORT, 10);
const mongodb = {
  url: MONGODB_URL,
  database: MONGODB_URL.split('/')[3].split('?')[0],
  options: {
    // disable deprecation warnings
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
  }
};
const app = loadApp();

logger.info(`[API] Starting server version: ${TAG}`);

mongoose.connect(
  mongodb.url,
  mongodb.options
);

mongoose.connection.on('connected', () => {
  logger.info(`[MDB] Connected to database: ${mongodb.database}`);
});

mongoose.connection.on('disconnected', () => {
  logger.warn(`[MDB] Disconnected from database: ${mongodb.database}`);
});

mongoose.connection.on('error', err => {
  logger.error(`[MDB] Database connection failed: ${mongodb.database}`);
  exitIfError(err, 'MDB');
});

app.listen(port, () => {
  logger.info(`[API] Server online at: 0.0.0.0:${port}`);
});
