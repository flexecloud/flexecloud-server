# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.12.0] - 2018-11-24

### Added

- Endpoint `POST /v1/auth/azure_ad` for Azure OpenID Connect callback

## [0.11.0] - 2018-11-24

### Added

- Endpoint `DELETE /v1/users/:id` to delete a single user by `id`

## [0.10.1] - 2018-11-23

### Fixed

- CI should fail if the version number in the `CHANGELOG.md` is not `Unreleased` and the git tag exists

## [0.10.0] - 2018-11-23

### Added

- Endpoint `GET /v1/users/:id` to get single user by either `id`, `card_id` or `email`

## [0.9.0] - 2018-11-20

### Added

- Integration of `card_id` from MS Graph API via custom attribute

## [0.8.0] - 2018-11-13

### Added

- Endpoint `PATCH /v1/users/:id` to edit a single user

### Changed

- `trim` middleware does not accept excluded fields any more
- `trim` middleware trims strings recursively

## [0.7.0] - 2018-11-08

### Added

- Endpoint `GET /v1/users` for a list of users

## [0.6.1] - 2018-11-05

### Fixed

- Post-deployment test delay has been increased to `20s` to ensure a stable reverse proxy setup before the test (#1)

## [0.6.0] - 2018-11-05

### Added

- Card ID property `card_number` to `user` model
- New environment variables
  - `AZURE_AD_ATTRIBUTE_CARD_NUMBER`

## [0.5.1] - 2018-11-02

### Changed

- Change internal structure of `req.user` object to make it compatible with all middlewares

## [0.5.0] - 2018-10-31

### Added

- Endpoint `POST /v1/issues` for creation of user feedback
- New environment variables
  - `GITLAB_API_URL`
  - `GITLAB_API_TOKEN`
  - `GITLAB_PROJECT`

## [0.4.1] - 2018-10-28

### Fixed

- Added missing environment variable `JWT_KEY` to `Dockerfile` and CI pipeline

## [0.4.0] - 2018-10-28

### Fixed

- Timestamp plugin applies timestamps now correctly on update and on creation

### Added

- Update `README.md` with license information and environment variables
- Authentification with [passport](http://www.passportjs.org/) via [Azure](https://azure.microsoft.com/en-us/)
- Endpoint `GET /v1/me` for own identity query
- Endpoint `GET /v1/auth/azure_ad` for Azure OpenID Connect login
- Endpoint `POST /v1/auth/azure_ad/hook` for Azure OpenID Connect callback
- New environment variables
  - `APP_URL`
  - `AZURE_AD_CLIENT_ID`
  - `AZURE_AD_CLIENT_SECRET`
  - `AZURE_AD_TENANT`
  - `AZURE_AD_COOKIE_KEY_32`
  - `AZURE_AD_COOKIE_KEY_12`
  - `AZURE_AD_DOMAIN`

### Changed

- Refactored `src/common/env.js` module to apply default values to environment variables and fix tests

## [0.3.1] - 2018-10-12

### Fixed

- Add delay to wait for deployment to settle before post-deployment test

## [0.3.0] - 2018-10-12

### Added

- Connect deployment to environment in GitLab

### Changed

- Property `uptime` has been replaced by the property `started_at`, which returns the ISO 8601 string of the start time

## [0.2.0] - 2018-10-11

### Added

- Run linter in CI
- Install dependencies via `npm ci` to enforce aligned `package-lock.json`

### Changed

- Improved tagging mechanism checks if version in `CHANGELOG.md` is aligned with `package.json` and `package-lock.json`

### Fixed

- Align versions in `package.json` and `package-lock.json` with `CHANGELOG.md`

## [0.1.2] - 2018-10-11

### Fixed

- Typo in [.gitlab-ci.yml](./.gitlab-ci.yml) causing deployments to fail

## [0.1.1] - 2018-10-11

### Fixed

- Unit tests in CI
- Fix tag in Docker build

## [0.1.0] - 2018-10-10

### Added

- [Express](https://expressjs.com) setup for common REST server
- Environment variables via `.env`
- Code formatting with [Prettier](https://prettier.io)
- Linting with [ESLint](https://eslint.org/)
- Testing via [Jest](https://jestjs.io/)
- Mocking with `babel-plugin-rewire` and `babel-jest`
- Express testing with `supertest`
- [MIT License](https://mit-license.org/)
- CI with `.gitlab-ci.yml`
- `mongoose` as ODM for `mongodb`
- JSON response for unsupported endpoint
- JSON response for unknown error
- Internationalization with `i18n`
- Endpoint `GET /v1/health`
- Automatic tagging via [CHANGELOG.md](./CHANGELOG.md)
