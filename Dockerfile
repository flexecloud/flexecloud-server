# base image
FROM node:8-alpine

# working directory
WORKDIR /usr/src/app

# copy dependency lists
COPY package.json package-lock.json ./

# install dependencies of node-gyp
RUN apk add --no-cache --virtual .gyp python make g++

# install dependencies
RUN npm install --production

# remove build dependencies
RUN apk del .gyp

# copy source files
COPY . ./

# configure static build variables
ARG TAG

# configure dynamic environment variables
ENV PORT=80
ENV MONGODB_URL=""
ENV APP_URL=""
ENV WEBAPP_URL=""
ENV JWT_KEY=""
ENV AZURE_AD_CLIENT_ID=""
ENV AZURE_AD_CLIENT_SECRET=""
ENV AZURE_AD_TENANT=""
ENV AZURE_AD_COOKIE_KEY_32=""
ENV AZURE_AD_COOKIE_KEY_12=""
ENV AZURE_AD_DOMAIN=""
ENV AZURE_AD_ATTRIBUTE_CARD_NUMBER=""
ENV AZURE_AD_ATTRIBUTE_CARD_ID=""
ENV GITLAB_API_URL=""
ENV GITLAB_API_TOKEN=""
ENV GITLAB_PROJECT=""
ENV TAG=${TAG}

# make port accessible
EXPOSE $PORT

# start application
CMD [ "node", "src/index" ]
