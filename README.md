# SDU - The Core - Server

A server to manage the data of the core manufacturing platform.

## Installation

Before you begin, create a new `.env` file with the following content:

```ini
# HTTP REST API port
PORT=9000
# application version tag
TAG=dev
# database connection string
MONGODB_URL=mongodb://localhost/test
# REST API application protocol, host and port
APP_URL=http://localhost:9000
# web application protocol, host and port
WEBAPP_URL=http://localhost:8080
# random string for JWT encryption
JWT_KEY=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# azure active directory app registration application id
AZURE_AD_CLIENT_ID=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxx
# azure active directory app registration key
AZURE_AD_CLIENT_SECRET=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# azure directory id
AZURE_AD_TENANT=xxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxxx
# 32 character cookie encryption key
AZURE_AD_COOKIE_KEY_32=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
# 12 character cookie encryption key
AZURE_AD_COOKIE_KEY_12=xxxxxxxxxxxx
# custom field in azure active directory that contains the card number
AZURE_AD_ATTRIBUTE_CARD_NUMBER=extension_xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx_cardNumber
# azure active directory domain
AZURE_AD_DOMAIN=example.com
# gitlab instance base URL
GITLAB_API_URL=gitlab.com
# personal gitlab access token
GITLAB_API_TOKEN=personalAccessToken
# gitlab group and project
GITLAB_PROJECT=sdu-thecore/sdu-thecore-docs
```

Make sure to have at least the latest LTS of [nodejs](https://nodejs.org/en/) installed, then run:

```shell
$ npm i
```

Once you have the dependencies installed, you can run:

```shell
$ npm start
```

Now point your browser to [http://localhost:9000/v1/health](http://localhost:9000/v1/health).

## Linting

In order to verify the code, you can run [ESLint](https://eslint.org) by typing:

```shell
$ npm run lint
```

## Formatting

If you are not using a plugin for your IDE to automatically format the code with [Prettier](https://prettier.io), you can run it via:

```shell
$ npm run format
```

## Testing

To run the unit test suite of this application, run:

```shell
$ npm run test:unit
```

## License

This project is licensed under the terms of the [MIT license](https://mit-license.org).
